"""Field-effect measurements with PPMS and source-measure unit."""
import sys
import logging
from importlib import import_module
# Always prefer setuptools over distutils
from setuptools import setup, find_packages, Command
from setuptools.command.build_py import build_py
# To use a consistent encoding
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))


class BuildUi(Command):
    user_options = [
        # The format is (long option, short option, description).
        ('force', None, 'Force flag, will force recompilation of every ui file'),
    ]

    def initialize_options(self):
        """Set default values for options."""
        # Each user option must be listed here with their default value.
        self.force = False

    def finalize_options(self):
        """Post-process options."""
        pass

    def run(self):
        from compileUi import compile_ui, logger
        logger.setLevel('INFO')
        logger.addHandler(logging.StreamHandler())
        compile_ui(self.force)


class BuildPyCommand(build_py):
    """Custom build command."""

    def run(self):
        # prevent pip from breaking conda environment
        # See: https://github.com/ContinuumIO/anaconda-issues/issues/1970
        _required_modules = ['slave', 'matplotlib', 'pandas', 'pyvisa', 'PyQt5']
        missing = []
        for module in _required_modules:
            try:
                import_module(module)
            except ImportError:
                missing.append(module)
        if missing:
            raise RuntimeError('Please install required libraries: %s' % missing)
        self.run_command('build_ui')
        super(build_py, self).run()



setup(
    name='fieldeffect',  # Required
    version='0.1.2',  # Required
    description='Field-effect measurements with PPMS and source-measure unit.',  # Required
    # long_description=long_description,  # Optional
    # url='https://github.com/pypa/sampleproject',  # Optional
    author='t-onoz',  # Optional
    packages=find_packages(exclude=['contrib', 'docs', 'tests']),  # Required
    package_data={'fieldeffect': ['sequence/*.json']},
    entry_points={  # Optional
        'gui_scripts': [
            'fieldeffect-noconsole=fieldeffect.qt.main:main',
        ],
        'console_scripts': [
            'fieldeffect-test-b2900=fieldeffect.tests.test_b2900:main',
            'fieldeffect-test-k2400=fieldeffect.tests.test_k2400:main',
            'fieldeffect-test=fieldeffect.tests.test_commands:main',
            'fieldeffect-editor=fieldeffect.qt.widgets.sequenceeditor:main',
            'fieldeffect=fieldeffect.qt.main:main',
            'fieldeffect-processdata=fieldeffect.qt.processdata:main',
        ],
    },
    cmdclass={
        'build_ui': BuildUi,
        'build_py': BuildPyCommand,
    }
)
