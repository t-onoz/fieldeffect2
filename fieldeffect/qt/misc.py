# -*- coding: utf-8 -*-
from logging import getLogger
from PyQt5 import QtWidgets


class catcher:
    def __init__(self, catch=Exception, msg='unkwon error', logger=None):
        self.msg = msg
        self.catch = catch
        self.logger = getLogger(__name__) if logger is None else logger

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, tb):
        if isinstance(exc_value, self.catch):
            self.logger.warning(self.msg, exc_info=(exc_type, exc_value, tb))
            QtWidgets.QMessageBox.warning(None, 'error', self.msg + '\n\n%r' % exc_value)
            return True
