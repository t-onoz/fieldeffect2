# -*- coding: utf-8 -*-
import sys
from logging import getLogger

import visa
from PyQt5 import QtWidgets

from fieldeffect import config
from fieldeffect.measurement import InstrumentBundle
from .ui.ui_connectiondialog import Ui_ConnectionDialog
from ..misc import catcher

logger = getLogger(__name__)


def _list_resources():
    resources = []
    try:
        rm = visa.ResourceManager()
    except Exception as e:
        logger.warning('Failed to open VISA resource manager: %s', e)
    else:
        resources += list(rm.list_resources())
    if ('linux' in sys.platform) and (not any('gpib' in r.lower() for r in resources)):
        resources += list('LinuxGpib-%d' % x for x in range(0, 31))
    return resources


class ConnectionDialog(QtWidgets.QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.connected = False
        self.bundle = InstrumentBundle()
        self.ui = Ui_ConnectionDialog()
        self.ui.setupUi(self)
        self.buttonOk = self.ui.buttonBox.button(QtWidgets.QDialogButtonBox.Ok)
        self.buttonOk.setEnabled(False)
        self.ui.checkPPMS.stateChanged.connect(self.ui.cbPPMS.setEnabled)
        self.ui.checkSD.stateChanged.connect(self.ui.cbSD.setEnabled)
        self.ui.checkGate.stateChanged.connect(self.ui.cbGate.setEnabled)
        self.ui.checkVolt0.stateChanged.connect(self.ui.cbVolt0.setEnabled)
        self.ui.checkVolt1.stateChanged.connect(self.ui.cbVolt1.setEnabled)
        self.ui.checkVolt2.stateChanged.connect(self.ui.cbVolt2.setEnabled)
        self.ui.checkVolt3.stateChanged.connect(self.ui.cbVolt3.setEnabled)
        self.ui.checkVolt4.stateChanged.connect(self.ui.cbVolt4.setEnabled)
        self.ui.pbConnect.clicked.connect(self.connect)
        try:
            resourcelist = _list_resources()
        except Exception:
            logger.warning('failed to obtain resource list.', exc_info=True)
        else:
            for child in self.children():
                if isinstance(child, QtWidgets.QComboBox):
                    child.addItems(resourcelist)
        self.ui.cbPPMS.setCurrentText(config.ADDR_PPMS)
        self.ui.cbSD.setCurrentText(config.ADDR_SD)
        self.ui.cbGate.setCurrentText(config.ADDR_GATE)
        self.ui.cbVolt0.setCurrentText(config.ADDR_VOLT0)
        self.ui.cbVolt1.setCurrentText(config.ADDR_VOLT1)
        self.ui.cbVolt2.setCurrentText(config.ADDR_VOLT2)
        self.ui.cbVolt3.setCurrentText(config.ADDR_VOLT3)
        self.ui.cbVolt4.setCurrentText(config.ADDR_VOLT4)

    def check_ok(self):
        if self.connected:
            self.buttonOk.setEnabled(True)
        else:
            self.buttonOk.setEnabled(False)

    def connect(self):
        msg = 'Connection to instrument(s) failed.'
        addrPpms = self.ui.cbPPMS.currentText() if self.ui.checkPPMS.isChecked() else None
        addrSd = self.ui.cbSD.currentText() if self.ui.checkSD.isChecked() else None
        addrGate = self.ui.cbGate.currentText() if self.ui.checkGate.isChecked() else None
        addrVolts = []
        for i in range(5):
            if getattr(self.ui, 'checkVolt%d' % i).isChecked():
                addrVolts.append(getattr(self.ui, 'cbVolt%d' % i).currentText())
        with catcher(Exception, msg, logger):
            self.bundle.connect(addrPpms, addrSd, addrGate, *addrVolts)
            QtWidgets.QMessageBox.information(self, '', 'connected to instruments.')
            self.connected = True
            logger.info(self.bundle.get_info())
        self.check_ok()


if __name__ == '__main__':
    app = QtWidgets.QApplication([])
    m = ConnectionDialog()
    ret = m.exec_()
    if ret == QtWidgets.QDialog.Accepted:
        print('hi')

