from PyQt5 import QtWidgets


def notify_error(msg, parent=None, logger=None):
    QtWidgets.QMessageBox.warning(parent, 'ERROR', msg)
    if logger:
        logger.info(msg)
        logger.debug('', exc_info=True)
