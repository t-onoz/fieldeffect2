# -*- coding: utf-8 -*-
import logging
from collections import Mapping

from PyQt5 import QtWidgets, QtCore

from .ui.ui_dictplotter import Ui_DictPlotter

try:
    import pandas as pd
except ImportError:
    dtypes = Mapping
    pd = None
else:
    dtypes = (Mapping, pd.DataFrame)


logger = logging.getLogger(__name__)


class DictPlotter(QtWidgets.QWidget):
    def __init__(self, parent=None, flags=QtCore.Qt.WindowFlags()):
        super(DictPlotter, self).__init__(parent, flags)
        self.ui = Ui_DictPlotter()
        self.ui.setupUi(self)
        self._data = dict()
        self.axes = self.ui.plotArea.figure.add_subplot(111)
        self.ui.plotArea.figure.set_tight_layout(True)
        self.ui.cbX.currentIndexChanged.connect(self.axes.cla)
        self.ui.cbX.currentIndexChanged.connect(self.plot)
        self.ui.cbY.currentIndexChanged.connect(self.axes.cla)
        self.ui.cbY.currentIndexChanged.connect(self.plot)
        self.ui.pbAutoX.clicked.connect(self.autoscale)
        self.ui.pbAutoY.clicked.connect(self.autoscale)
        self.ui.sbMaxPoints.valueChanged.connect(self.plot)

    def load_data(self, data):
        if not isinstance(data, dtypes):
            logger.warning('possibly invalid data type: \'%s\'', type(data).__name__, stack_info=True)
        columns_old = list(self._data)
        columns_new = list(data)
        self._data = data
        if columns_new != columns_old:
            # data structure changed
            self.ui.cbX.clear()
            self.ui.cbY.clear()
            for c in columns_new:
                self.ui.cbX.addItem(str(c), c)
                self.ui.cbY.addItem(str(c), c)
        self.plot()

    def autoscale(self, *_, axis='both'):
        sender = self.sender()
        if sender is self.ui.pbAutoX:
            axis = 'x'
        elif sender is self.ui.pbAutoY:
            axis = 'y'
        self.axes.autoscale(axis=axis)
        self.ui.plotArea.canvas.draw()

    def plot(self, *_, max_points=None):
        if max_points is None:
            max_points = self.ui.sbMaxPoints.value()
        x = self.ui.cbX.currentData(QtCore.Qt.UserRole)
        y = self.ui.cbY.currentData(QtCore.Qt.UserRole)
        if not (x in self._data and y in self._data):
            return
        if pd is not None and isinstance(self._data, pd.DataFrame):
            idx = self.ui.cbX.currentIndex()
            idy = self.ui.cbY.currentIndex()
            xs = self._data.iloc[-max_points:, idx]
            ys = self._data.iloc[-max_points:, idy]
        else:
            xs = self._data[x][-max_points:]
            ys = self._data[y][-max_points:]
        xlabel = self.ui.cbX.currentText()
        ylabel = self.ui.cbY.currentText()
        try:
            if self.axes.lines:
                self.axes.lines[0].set_data(xs, ys)
            else:
                self.axes.plot(xs, ys)
            self.axes.relim()
            self.axes.autoscale(None)
            self.axes.set_xlabel(xlabel)
            self.axes.set_ylabel(ylabel)
            self.ui.plotArea.canvas.draw()
        except OverflowError:
            logger.debug('overflow error on DictPlotter.plot', exc_info=True)
            self.axes.cla()
            t = self.axes.text(0.5, 0.5, 'OverflowError',
                           ha='center', va='center',
                           transform=self.axes.transAxes)
            self.ui.plotArea.canvas.draw()
            t.remove()
        except Exception:
            logger.info('plot (%r vs %r) failed!', x, y)
            logger.debug('', exc_info=True)


if __name__ == '__main__':
    import timeit
    from itertools import cycle
    import numpy as np
    logging.basicConfig()
    # logger.setLevel('DEBUG')
    app = QtWidgets.QApplication([])

    def newdata(shape=(1000000, 4), scale=(1, 1e10)):
        x = 10 ** np.random.uniform(*np.log10(scale))
        return pd.DataFrame(x * np.random.random(shape), columns=np.arange(shape[1]), dtype=object)

    mydata = [newdata() for _ in range(10)]

    m = DictPlotter()
    m.resize(640, 480)
    m.show()

    c = cycle(mydata)

    timer = QtCore.QTimer()
    timer.setInterval(1000)
    timer.timeout.connect(
        lambda: print(
            timeit.timeit('m.load_data(next(c))', number=1, globals=globals())
        )
    )
    timer.start()

    app.exec_()
