# -*- coding: utf-8 -*-
from logging import getLogger
from PyQt5 import QtWidgets, QtGui

from fieldeffect import command, config
from .validator import ValLineEdit
from .ui.ui_comment import Ui_Comment
from .ui.ui_wait import Ui_Wait
from .ui.ui_applygatevoltage import Ui_ApplyGateVoltage
from .ui.ui_scantemperature import Ui_ScanTemperature
from .ui.ui_scanfield import Ui_ScanField
from .ui.ui_shutdown import Ui_Shutdown
from .ui.ui_setsourcedrain import Ui_SetSourceDrain
from .ui.ui_scaniv import Ui_ScanIV

logger = getLogger(__name__)


class WrongInputError(ValueError): pass


class CommandBase(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self._cls = getattr(command, self.__class__.__name__)   # type: command.CommandBase

    def getParameters(self):
        p = dict()
        for key, widget in self.inputs.items():
            if hasattr(widget, 'hasAcceptableInput') and not widget.hasAcceptableInput():
                return None
            if isinstance(widget, QtWidgets.QLineEdit):
                v = widget.validator()
                if isinstance(v, QtGui.QIntValidator):
                    p[key] = int(widget.text())
                elif isinstance(v, QtGui.QDoubleValidator):
                    p[key] = float(widget.text())
                else:
                    p[key] = widget.text()
            elif isinstance(widget, QtWidgets.QComboBox):
                p[key] = widget.currentText()
            elif isinstance(widget, (QtWidgets.QSpinBox, QtWidgets.QDoubleSpinBox)):
                p[key] = widget.value()
            elif isinstance(widget, QtWidgets.QCheckBox):
                p[key] = widget.isChecked()
        return p

    def setParameters(self, d: dict):
        v_mode = d.pop('mode', None)
        if v_mode is not None:
            self.inputs['mode'].setCurrentText(str(v_mode))
        for k, v in d.items():
            widget = self.inputs[k]
            if isinstance(widget, QtWidgets.QLineEdit):
                widget.setText(str(v))
            elif isinstance(widget, QtWidgets.QComboBox):
                widget.setCurrentText(str(v))
            elif isinstance(widget, (QtWidgets.QSpinBox, QtWidgets.QDoubleSpinBox)):
                widget.setValue(v)
            elif isinstance(widget, QtWidgets.QCheckBox):
                widget.setChecked(v)

    def as_command(self) -> command.CommandBase:
        p = self.getParameters()
        if p:
            return self._cls(**p)

    def update_from_command(self, com: command.CommandBase):
        if isinstance(com, self._cls):
            p = com.parameters
            self.setParameters(p)

    def _adapt_colors(self):
        for le in self.findChildren(ValLineEdit):
            le.adapt_color()


class Comment(CommandBase):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_Comment()
        self.ui.setupUi(self)
        self.inputs = {'comment': self.ui.leComment}


class Wait(CommandBase):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_Wait()
        self.ui.setupUi(self)
        self.inputs = {'total': self.ui.sbTotal, 'interval': self.ui.sbInterval}


class ApplyGateVoltage(CommandBase):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_ApplyGateVoltage()
        self.ui.setupUi(self)
        self.ui.leTarget.setValidator(QtGui.QDoubleValidator(-config.VMAX, config.VMAX, 3, self))
        self.ui.leStep.setValidator(QtGui.QDoubleValidator(0, config.VMAX, 4, self))
        self.ui.leCompliance.setValidator(QtGui.QDoubleValidator(0, config.IMAX, 12, self))
        self.inputs = {
            'target': self.ui.leTarget,
            'step': self.ui.leStep,
            'interval': self.ui.sbInterval,
            'is_3e': self.ui.cbIs3e,
            'compliance': self.ui.leCompliance,
        }
        self._adapt_colors()


class ScanTemperature(CommandBase):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_ScanTemperature()
        self.ui.setupUi(self)
        self.ui.leTarget.setValidator(QtGui.QDoubleValidator(1.9, 350, 3, self))
        self.ui.leRate.setValidator(QtGui.QDoubleValidator(0, 20, 3, self))
        self.inputs = {
            'target': self.ui.leTarget,
            'interval': self.ui.sbInterval,
            'rate': self.ui.leRate,
            'mode': self.ui.cbMode,
        }
        self._adapt_colors()


class ScanField(CommandBase):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_ScanField()
        self.ui.setupUi(self)
        self.ui.leTarget.setValidator(QtGui.QDoubleValidator(-90000, 90000, 2, self))
        self.ui.leRate.setValidator(QtGui.QDoubleValidator(0, 100, 2, self))
        self.inputs = {
            'interval': self.ui.sbInterval,
            'target': self.ui.leTarget,
            'rate': self.ui.leRate,
            'approach': self.ui.cbApproach,
            'mode': self.ui.cbMode,
        }
        self._adapt_colors()


class Shutdown(CommandBase):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_Shutdown()
        self.ui.setupUi(self)
        self.inputs = {
            'stop_ppms': self.ui.cbStopPpms,
            'stop_sd': self.ui.cbStopSd,
            'stop_gate': self.ui.cbStopGate,
        }
        self._adapt_colors()


class SetSourceDrain(CommandBase):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_SetSourceDrain()
        self.ui.setupUi(self)
        self.ui.leLevel.setValidator(QtGui.QDoubleValidator(-config.VMAX, config.VMAX, 9, self))
        self.ui.leCompliance.setValidator(QtGui.QDoubleValidator(0, config.IMAX, 12, self))
        self.ui.cbMode.currentIndexChanged.connect(self._change_ui)
        self._change_ui()
        self.inputs = {
            'mode': self.ui.cbMode,
            'level': self.ui.leLevel,
            'compliance': self.ui.leCompliance,
            'four_wire': self.ui.cbFourWire,
        }
        self._adapt_colors()

    def _change_ui(self):
        is_volt = self.ui.cbMode.currentText().lower().startswith('volt')
        val_level = (-config.VMAX, config.VMAX, 9) if is_volt else (-config.IMAX, config.IMAX, 12)
        val_comp = (0, config.IMAX, 12) if is_volt else (0, config.VMAX, 9)
        u_level = 'V' if is_volt else 'A'
        u_comp = 'A' if is_volt else 'V'
        def_level = 0.1 if is_volt else 1e-3
        def_comp = 0.01 if is_volt else 10
        self.ui.leLevel.validator().setRange(*val_level)
        self.ui.leLevel.setText(str(def_level))
        self.ui.leCompliance.validator().setRange(*val_comp)
        self.ui.leCompliance.setText(str(def_comp))
        self.ui.lbUnitLevel.setText(u_level)
        self.ui.lbUnitCompliance.setText(u_comp)
        self._adapt_colors()


class ScanIV(CommandBase):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_ScanIV()
        self.ui.setupUi(self)
        self.ui.leStart.setValidator(QtGui.QDoubleValidator(self))
        self.ui.leStop.setValidator(QtGui.QDoubleValidator(self))
        self.ui.leCompliance.setValidator(QtGui.QDoubleValidator(self))
        self.ui.cbMode.currentIndexChanged.connect(self._change_ui)
        self.ui.cbGoBack.setChecked(True)
        self._change_ui()
        self.inputs = {
            'mode': self.ui.cbMode,
            'four_wire': self.ui.cbFourWire,
            'start': self.ui.leStart,
            'stop': self.ui.leStop,
            'points': self.ui.cbPoints,
            'compliance': self.ui.leCompliance,
            'go_back': self.ui.cbGoBack
        }
        self._adapt_colors()

    def _change_ui(self):
        is_volt = self.ui.cbMode.currentText().lower().startswith('v')
        val_level = (-config.VMAX, config.VMAX, 9) if is_volt else (-config.IMAX, config.IMAX, 12)
        val_comp = (0, config.IMAX, 12) if is_volt else (0, config.VMAX, 9)
        u_level = 'V' if is_volt else 'A'
        u_comp = 'A' if is_volt else 'V'
        def_comp = 0.01 if is_volt else 10
        self.ui.leStart.validator().setRange(*val_level)
        self.ui.leStop.validator().setRange(*val_level)
        self.ui.leCompliance.validator().setRange(*val_comp)
        self.ui.leCompliance.setText(str(def_comp))
        self.ui.lbUnitStart.setText(u_level)
        self.ui.lbUnitStop.setText(u_level)
        self.ui.lbUnitCompliance.setText(u_comp)
        self._adapt_colors()


if __name__ == '__main__':
    import sys
    from inspect import isclass
    app = QtWidgets.QApplication([])
    widgets = [g for g in globals().values() if isclass(g) and issubclass(g, CommandBase) and g is not CommandBase]
    if sys.argv[1:]:
        widgets = [globals()[k] for k in sys.argv[1:]]
    for cls in widgets:
        w = cls()
        w.show()
        app.exec_()
        c = w.as_command()
        if c:
            print(c)
            w2 = cls()
            w2.update_from_command(c)
            w2.show()
            app.exec_()

