# -*- coding: utf-8 -*-
import pandas as pd
from PyQt5 import QtWidgets, QtCore

from ..models import PandasModel


class PandasView(QtWidgets.QTableView):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setModel(PandasModel(parent=self))
        self.setSelectionMode(QtWidgets.QTableView.ContiguousSelection)
        self.copyAction = QtWidgets.QAction('Copy', self)
        self.copyAction.triggered.connect(self.copy_selected)
        self.setContextMenuPolicy(QtCore.Qt.ActionsContextMenu)
        self.addAction(self.copyAction)

    def get_selected(self) -> pd.DataFrame:
        indexes = self.selectionModel().selectedIndexes()
        rows = set(i.row() for i in indexes)
        columns = set(i.column() for i in indexes)
        return self.model().get_dframe().iloc[sorted(rows), sorted(columns)]

    def copy_selected(self):
        self.get_selected().to_clipboard(excel=True, index=False)

if __name__ == '__main__':
    from itertools import cycle
    import timeit
    import numpy as np

    maxsize = 1e6

    def newframe():
        scale = 10 ** np.random.uniform(0, 10)
        rows = int(10 ** np.random.uniform(0, np.log10(maxsize)))
        cols = int(maxsize) // rows
        return pd.DataFrame(scale * np.random.random((rows, cols)), dtype=object)

    mydata = [newframe() for _ in range(10)]
    print('---------- shapes ----------')
    print(*(frame.shape for frame in mydata), sep='\n')
    c = cycle(mydata)

    timer = QtCore.QTimer(None)
    timer.setInterval(1000)
    app = QtWidgets.QApplication([])
    view = PandasView()
    model = view.model()
    timer.timeout.connect(
        lambda: print(
            timeit.timeit('model.set_dframe(next(c))', number=1, globals=globals())
        )
    )
    timer.start()
    view.show()
    app.exec_()
