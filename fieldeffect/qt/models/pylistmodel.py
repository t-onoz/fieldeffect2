from PyQt5 import QtCore
from PyQt5.QtCore import Qt


class PyListModel(QtCore.QAbstractListModel):
    def __init__(self, iterable=None, parent=None):
        super().__init__(parent)
        self._list = [] if iterable is None else list(iterable)

    def rowCount(self, parent=QtCore.QModelIndex()):
        if parent.isValid():
            return 0
        return len(self._list)

    def data(self, index: QtCore.QModelIndex, role=Qt.DisplayRole):
        if not index.isValid():
            return QtCore.QVariant()
        if role == Qt.DisplayRole:
            return str(self._list[index.row()])
        if role == Qt.UserRole:
            return self._list[index.row()]
        return QtCore.QVariant()

    def setData(self, index: QtCore.QModelIndex, value, role=Qt.EditRole):
        if not index.isValid():
            return False
        if role == Qt.UserRole:
            self._list[index.row()] = value
            self.dataChanged.emit(index, index)
        return True

    def itemData(self, index: QtCore.QModelIndex):
        if not index.isValid():
            return super().itemData(index)
        d = super().itemData(index)
        d[Qt.UserRole] = self.data(index, Qt.UserRole)
        return d

    def flags(self, index: QtCore.QModelIndex):
        if index.isValid():
            return Qt.ItemIsSelectable | Qt.ItemIsDragEnabled | Qt.ItemIsEnabled
        return Qt.ItemIsSelectable | Qt.ItemIsDragEnabled | Qt.ItemIsDropEnabled | Qt.ItemIsEnabled

    def insertRows(self, row: int, count: int, parent=QtCore.QModelIndex()) -> bool:
        first = row
        if count <= 0 or first < 0 or first > self.rowCount():
            return False
        self.beginInsertRows(parent, row, row + count - 1)
        for _ in range(count):
            self._list.insert(row, None)
        self.endInsertRows()
        return True

    def removeRows(self, row: int, count: int, parent=QtCore.QModelIndex()) -> bool:
        first = row
        last = row + count - 1
        if count <= 0 or first < 0 or last >= self.rowCount():
            return False
        self.beginRemoveRows(parent, first, last)
        for _ in range(count):
            self._list.pop(row)
        self.endRemoveRows()
        return True

    def moveRows(self, sourceParent: QtCore.QModelIndex, sourceRow: int, count: int, destParent: QtCore.QModelIndex, destChild: int) -> bool:
        first = sourceRow
        last = sourceRow + count - 1
        if count <= 0 or first < 0 or last >= self.rowCount() or destChild < 0 or destChild >= self.rowCount():
            return False
        if first <= destChild <= last + 1:
            return False
        self.beginMoveRows(sourceParent, first, last, destParent, destChild)
        to_move = self._list[sourceRow:sourceRow+count]
        self._list[sourceRow:sourceRow+count] = []
        if destChild < sourceRow:
            dest = destChild
        else:
            dest = destChild - count
        self._list[dest:dest] = to_move
        self.endMoveRows()
        return True

    def supportedDragActions(self):
        return QtCore.Qt.CopyAction | QtCore.Qt.MoveAction

    def supportedDropActions(self):
        return QtCore.Qt.CopyAction | QtCore.Qt.MoveAction

    def __iter__(self):
        return iter(self._list)

    def __contains__(self, item):
        return item in self._list

    def __getitem__(self, item):
        if isinstance(item, slice):
            return PyListModel(self._list[item], parent=self.parent())
        else:
            return self._list[item]

    def __len__(self):
        return len(self._list)
    
    def indexOf(self, *a, **kw):
        return self._list.index(*a, **kw)

    def count(self, x):
        return self._list.count(x)

    def __setitem__(self, item, value):
        if isinstance(item, slice):
            self.layoutAboutToBeChanged.emit()
            self._list[item] = value
            self.layoutChanged.emit()
        elif isinstance(item, int):
            item = self.rowCount() + item if item < 0 else item
            if not 0 <= item < self.rowCount():
                raise IndexError('list index out of range')
            self.setData(self.index(item), value, QtCore.Qt.UserRole)
        else:
            raise TypeError('list indices must be integers or slices, not %s' %  type(item).__name__)
            
    def __delitem__(self, item):
        if isinstance(item, slice):
            self.layoutAboutToBeChanged.emit()
            del self._list[item]
            self.layoutChanged.emit()
        elif isinstance(item, int):
            item = self.rowCount() + item if item < 0 else item
            if not 0 <= item < self.rowCount():
                raise IndexError('list index out of range')
            self.removeRow(item)
        else:
            raise TypeError('list indices must be integers or slices, not %s' % type(item).__name__)
            
    def append(self, value):
        row = self.rowCount()
        self.insertRow(row)
        self.setData(self.index(row), value, QtCore.Qt.UserRole)
    
    def clear(self):
        self.removeRows(0, self.rowCount())
    
    def extend(self, iterable):
        for v in iterable:
            self.append(v)

    def insert(self, i, x):
        if not isinstance(i, int):
            raise TypeError("'%s' object cannot be interpreted as an integer" % type(i).__name__)
        i = self.rowCount() + i if i < 0 else i
        i = max(0, i)
        i = min(i, len(self))
        self.insertRow(i)
        self.setData(self.index(i), x, QtCore.Qt.UserRole)

    def pop(self, i=-1):
        if not isinstance(i, int):
            raise TypeError('integer argument expected, got %s' % type(i).__name__)
        if self.rowCount() == 0:
            raise IndexError('pop from empty list')
        i = self.rowCount() + i if i < 0 else i
        if not 0 <= i < self.rowCount():
            raise IndexError('list index out of range')
        item = self._list[i]
        self.removeRow(i)
        return item

    def remove(self, x):
        for i, v in enumerate(self):
            if v == x:
                self.removeRow(i)
                return
        raise ValueError('in PyListModel.remove(x): x not in list')
    
    def reverse(self):
        self.layoutAboutToBeChanged.emit()
        self._list.reverse()
        self.layoutChanged.emit()

    def copy(self):
        return PyListModel(self, parent=self.parent())

    def sort(self, *, key=None, reverse=False):
        self.layoutAboutToBeChanged.emit([], QtCore.QAbstractItemModel.VerticalSortHint)
        self._list.sort(key=key, reverse=reverse)
        self.layoutChanged.emit()

    def __repr__(self):
        cls = self.__class__.__name__
        return '%s(%r)' % (cls, self._list)
