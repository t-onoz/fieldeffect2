# -*- coding: utf-8 -*-
import pandas as pd
from PyQt5 import QtCore


class PandasModel(QtCore.QAbstractTableModel):
    def __init__(self, data: pd.DataFrame=None, parent=None):
        super().__init__(parent)
        if data is None:
            data = pd.DataFrame()
        if not isinstance(data, pd.DataFrame):
            raise TypeError('invalid data type: \'%s\'' % type(data).__name__)
        self._data = data

    def rowCount(self, parent=QtCore.QModelIndex()):
        return self._data.index.size

    def columnCount(self, parent=QtCore.QModelIndex()):
        return self._data.columns.size

    def data(self, index, role=QtCore.Qt.DisplayRole):
        if index.isValid():
            row, col = index.row(), index.column()
            try:
                value = self._data.iloc[row, col]
            except IndexError:
                return QtCore.QVariant()
            if role == QtCore.Qt.DisplayRole:
                return str(value)
            elif role == QtCore.Qt.UserRole:
                return value
        return QtCore.QVariant()

    def headerData(self, section, orientation, role=QtCore.Qt.DisplayRole):
        if section < 0:
            return QtCore.QVariant()
        if role == QtCore.Qt.DisplayRole:
            if orientation == QtCore.Qt.Horizontal:
                try:
                    return str(self._data.columns[section])
                except IndexError:
                    return QtCore.QVariant()
            elif orientation == QtCore.Qt.Vertical:
                try:
                    return str(self._data.index[section])
                except IndexError:
                    return QtCore.QVariant()
        return super().headerData(section, orientation, role)

    def get_dframe(self):
        return self._data

    def set_dframe(self, df: pd.DataFrame):
        self.layoutAboutToBeChanged.emit()
        self._data = df
        self.layoutChanged.emit()
