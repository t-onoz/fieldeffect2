# -*- coding: utf-8 -*-
from PyQt5 import QtWidgets
import os
import pandas as pd


def _get_input(prompt, type_):
    while True:
        i = input(prompt)
        try:
            return type_(i)
        except Exception:
            print('invalid input. try again')


def average(a, b):
    return (a + b) / 2


def diffaverage(a, b):
    return (a - b) / 2


funcs = {'+': average, '-': diffaverage}


def main():
    app = QtWidgets.QApplication([])
    import traceback
    try:
        path, _ = QtWidgets.QFileDialog.getOpenFileName(None, 'select file', '', 'Data files (*.dat);; All files (*)')
        if not path:
            return
        df = pd.read_csv(path, delim_whitespace=True)
        functions = {
            'temperature': average,
            'time': average,
            'field': average,
            'V_SD': diffaverage,
            'I_SD': diffaverage,
            'V_G': average,
            'I_G': average,
        }
        for column in df.columns:
            if column.lower().startswith('voltmeter'):
                functions[column] = _get_input('process type for "%s" (+ or -): ' % column, lambda x: funcs[x])

        new = pd.DataFrame(columns=df.columns)
        for c in df.columns:
            if not c.startswith('R'):
                array = df[c].as_matrix()
                if len(array) % 2 == 1:
                    array = array[:-1]
                new[c] = functions[c](array[::2], array[1::2])
        new['R_SD'] = new['V_SD'] / new['I_SD']
        new['R_G'] = new['V_G'] / new['I_G']
        dest = os.path.splitext(path)[0] + '_processed.dat'
        new.to_csv(dest, index=None, sep='\t')
        QtWidgets.QMessageBox.information(None, 'saved.', 'File saved as: %s' % dest)
    except Exception as e:
        traceback.print_exc()
        QtWidgets.QMessageBox.critical(None, 'error', '%s' % e)


if __name__ == '__main__':
    main()
