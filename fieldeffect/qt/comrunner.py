# -*- coding: utf-8 -*-
"""command and data handling"""
import sys
import logging
import numpy as np
import pandas as pd
import gc
from io import StringIO
from slave import transport
from PyQt5 import QtCore
from fieldeffect import config
from fieldeffect.command import CommandBase
from fieldeffect.utils import retry

logger = logging.getLogger(__name__)


class AbortSignal(Exception):
    # for aborting sequence
    pass


class CommandRunner(QtCore.QObject):
    dataPointObtained = QtCore.pyqtSignal(np.ndarray)
    commandStarted = QtCore.pyqtSignal(int, int, CommandBase)  # idx, len(comlist), command
    commandFinished = QtCore.pyqtSignal(int, int, CommandBase, tuple)  # idx, len(comlist), command, exc_info
    allFinished = QtCore.pyqtSignal(list)  # command list

    def __init__(self, bundle, parent=None):
        super().__init__(parent)
        self.bundle = bundle
        self._abort_flag = False
        self.is_running = False

        # setup logging
        self.commandStarted.connect(lambda i, n, com: logger.info('Command #%d started: %s', i, com))
        self.commandFinished.connect(self.log_finish)
        self.allFinished.connect(lambda: logger.info('All commands finished.'))

    def abort(self):
        """Stop currently running measurements."""
        self._abort_flag = True

    def operation(self):
        # perform measurements, save values, etc.
        QtCore.QCoreApplication.processEvents()  # receive signals
        if self._abort_flag:
            raise AbortSignal
        if config.INVERT:
            r1, r2 = self.bundle.measure_bipolar()
            self.dataPointObtained.emit(r1)
            self.dataPointObtained.emit(r2)
        else:
            r = self.bundle.measure()
            self.dataPointObtained.emit(r)

    @QtCore.pyqtSlot(object)
    def run_all(self, comlist):
        self.is_running = True
        self._abort_flag = False
        try:
            comlist = [c for c in comlist]  # make a copy of original command list
            for idx, c in enumerate(comlist):
                QtCore.QCoreApplication.processEvents()
                if self._abort_flag:
                    break
                self.commandStarted.emit(idx, len(comlist), c)
                try:
                    retry(catch=Exception, exclude=AbortSignal,
                          onerror=self.bundle.clear_all)(c.run)(self.bundle, self.operation)
                except AbortSignal:
                    break
                except Exception as e:
                    self.commandFinished.emit(idx, len(comlist), c, sys.exc_info())
                else:
                    self.commandFinished.emit(idx, len(comlist), c, sys.exc_info())
        finally:
            self.is_running = False
            self._abort_flag = False
            self.allFinished.emit(comlist)

    def log_finish(self, i, n, command, exc_info):
        if exc_info[0] is None:
            logger.info('Command #%d finished', i)
        else:
            logger.warning('Command #%d (%s) failed!', i, command, exc_info=exc_info)


class DataHandler(QtCore.QObject):
    DELIMITER = '\t'
    dataRefreshed = QtCore.pyqtSignal(pd.DataFrame)

    def __init__(self, headers, parent=None):
        super().__init__(parent)
        self.dataframe = pd.DataFrame(columns=headers, dtype=float)
        self._buffer = StringIO()
        self.destroyed.connect(self.close_file)
        self._print = False

    def append_row(self, array):
        new = pd.DataFrame([array], columns=self.dataframe.columns)
        self.dataframe = self.dataframe.append(new, ignore_index=True)
        if self.dataframe.index.size >= config.ROWLIMIT:
            self.dataframe = self.dataframe.iloc[-int(0.9 * config.ROWLIMIT):, :].copy()
        gc.collect()
        self._buffer.write(self.DELIMITER.join('%.18e' % v for v in array))
        self._buffer.write('\n')
        self.dataRefreshed.emit(self.dataframe)
        if self._print:
            print(*array, sep=self.DELIMITER)

    def open_file(self, file, mode='a'):
        self.close_file()
        text = self._buffer.getvalue() if isinstance(self._buffer, StringIO) else ''
        fp = file if hasattr(file, 'write') else open(file, mode, buffering=1, encoding='utf-8')
        fp.write(self.DELIMITER.join(str(c) for c in self.dataframe.columns))
        fp.write('\n')
        fp.write(text)
        self._buffer.close()
        self._buffer = fp
    
    def close_file(self):
        if not isinstance(self._buffer, StringIO):
            self._buffer.close()
            self._buffer = StringIO()

    def print_to_screen(self):
        self._print = True
