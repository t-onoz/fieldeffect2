# -*- coding: utf-8 -*-
VMAX = 42  # in V
IMAX = 10  # in A

# integration time in number of power line cycles (i.e. 1/50s)
# 1以上にすることでAC由来のノイズが防げる
# 参考： https://www.keysight.com/main/editorial.jspx?cc=JP&lc=jpn&ckey=710001-2-jpn&nid=-33256.0.00&id=710001-2-jpn
NPLC = 2

# ゲート電流の上限値（ショートした時に大電流が流れるのを防ぐ）
IG_LIMIT = 5e-3

# VISA addresses
ADDR_SD = ADDR_GATE = 'GPIB0::23::INSTR'
ADDR_PPMS = 'GPIB0::15::INSTR'
ADDR_VOLT0 = 'GPIB0::16::INSTR'
ADDR_VOLT1 = ''
ADDR_VOLT2 = ''
ADDR_VOLT3 = ''
ADDR_VOLT4 = ''

# delay time between source on and measurement
TRIGGER_DELAY = 0.1

# maximum number of data points
ROWLIMIT = 20000

# invert source-drain for each measurement
INVERT = True
